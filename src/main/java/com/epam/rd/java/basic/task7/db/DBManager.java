package com.epam.rd.java.basic.task7.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

import static java.sql.DriverManager.getConnection;

public class DBManager {

	private static DBManager instance = null;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}
	//done
	public List<User> findAllUsers() throws DBException {
		Connection connection = null;
		List<User> result = new ArrayList<>();
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM users;");
				while (resultSet.next()){
					String login = resultSet.getString("login");
					result.add(new User(login));
				}
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return result;
	}
	//done
	public boolean insertUser(User user) throws DBException {
		Connection connection = null;
		int id;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				PreparedStatement statement = connection.prepareStatement("INSERT INTO users(login) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
				statement.setString(1, user.getLogin());
				id = statement.executeUpdate();
				ResultSet rs = statement.getGeneratedKeys();
				if (rs.next()){
					user.setId(rs.getInt(1));
				}
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return (id !=0);
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		int id = 0;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE login = ?");
				for (User user: users) {
				statement.setString(1, user.getLogin());
				id = statement.executeUpdate();
				}
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return (id !=0);
	}
	//done
	public User getUser(String login) throws DBException {
		Connection connection = null;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT * FROM users WHERE login = '"+login+"';");
				if (rs.next()) {
					User result = new User(rs.getString("login"));
					result.setId(rs.getInt("id"));
					return result;
				}
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return null;
	}
	//done
	public Team getTeam(String name) throws DBException {
		Connection connection = null;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				Statement statement = connection.createStatement();
				ResultSet rs = statement.executeQuery("SELECT * FROM teams WHERE name = '" + name + "';");
				if (rs.next()) {
					Team result = new Team(rs.getString("name"));
					result.setId(rs.getInt("id"));
					return result;
				}
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return null;
	}
	//done
	public List<Team> findAllTeams() throws DBException {
		Connection connection = null;
		List<Team> result = new ArrayList<>();
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM teams;");
				while (resultSet.next()){
					String name = resultSet.getString("name");
					result.add(new Team(name));
				}
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return result;
	}
	//done
	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		int id;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				PreparedStatement statement = connection.prepareStatement("INSERT INTO teams(name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
				statement.setString(1, team.getName());
				id = statement.executeUpdate();
				ResultSet rs = statement.getGeneratedKeys();
				if (rs.next()){
					team.setId(rs.getInt(1));
				}
				System.out.println("Team in insertTeam: " + team);
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return (id != 0);
	}
	//done
	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection =  null;
		int id = 0;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				connection.setAutoCommit(false);
				PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO users_teams(user_id, team_id) VALUES (?,?)");
				for (Team team : teams) {
					preparedStatement.setInt(1, user.getId());
					preparedStatement.setInt(2, team.getId());
					id = preparedStatement.executeUpdate();
				}
				connection.commit();
			} catch (Exception ex) {
				if (connection != null) {
					connection.rollback();
				}
				throw new DBException(ex.getMessage(), new Throwable());
			} finally {
				if (connection != null) {
					connection.close();
				}
			}
		} catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return (id != 0);
	}
	//done
	public List<Team> getUserTeams(User user) throws DBException {
		Connection connection = null;
		List<Team> result = new ArrayList<>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root", "root");
				Statement statement = connection.createStatement();
				String selectTeamsId = "SELECT team_id FROM users_teams WHERE user_id = " + getUser(user.getLogin()).getId();
				ResultSet rs = statement.executeQuery(selectTeamsId);
				String selectTeams = "SELECT name FROM teams WHERE id = ?";
				PreparedStatement preparedStatement = connection.prepareStatement(selectTeams);
				while (rs.next()){
					preparedStatement.setInt(1, rs.getInt("team_id"));
					ResultSet rs2 = preparedStatement.executeQuery();
					if (rs2.next()) {
						result.add(getTeam(rs2.getString("name")));
					}
				}
			} catch (Exception ex) {
				throw new DBException(ex.getMessage(), new Throwable());
			} finally {
				if (connection != null) {
					connection.close();
				}
			}
		} catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		System.out.println("ResultList: " + result);
		return result;
	}
	//done
	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		int id;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				PreparedStatement statement = connection.prepareStatement("DELETE FROM teams WHERE name = ?");
				statement.setString(1, team.getName());
				id = statement.executeUpdate();
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return (id !=0);
	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		int id;
		try{
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			try {
				connection = getConnection("jdbc:mysql://localhost/testdb?user=root&password=root","root","root");
				PreparedStatement statement = connection.prepareStatement("UPDATE teams SET name = ? WHERE id = " + team.getId());
				statement.setString(1, team.getName());
				id = statement.executeUpdate();
			}
			catch(Exception ex){
				throw new DBException(ex.getMessage(), new Throwable());
			}
			finally {
				if (connection != null) connection.close();
			}
		}
		catch(Exception ex){
			throw new DBException(ex.getMessage(), new Throwable());
		}
		return (id !=0);
	}
}