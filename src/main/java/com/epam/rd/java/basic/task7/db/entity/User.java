package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public User(String login){
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return login;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof User) {
			User other = (User) obj;
			return (this.getLogin().equals(other.getLogin()));
		} else return false;
	}
}