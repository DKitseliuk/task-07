package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public Team(String name){
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override
	public String toString() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Team){
			Team other = (Team) obj;
			return (this.getName().equals(other.getName()));
		} else return false;
	}
}